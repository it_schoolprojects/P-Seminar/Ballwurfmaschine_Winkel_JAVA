package gui;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class Console {

    @NotNull
    private final ObservableList<String> console = FXCollections.observableArrayList();
    @NotNull
    private final ListView<String> view;

    Console(@NotNull final ListView<String> view) {
        view.setItems(console);
        this.view = view;
    }

    public void print(@Nullable final String s) {
        Platform.runLater(() -> {
            if (s == null)
                console.add("");
            else
                console.add(s);
            view.scrollTo(Integer.MAX_VALUE);
        });
    }
}