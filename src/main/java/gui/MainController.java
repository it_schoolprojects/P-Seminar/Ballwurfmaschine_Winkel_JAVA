package gui;

import core.Algorithms;
import core.PointNotReachableException;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.jetbrains.annotations.Nullable;

public class MainController {

    @FXML
    private TextField startHeight;
    @FXML
    private TextField endHeight;
    @FXML
    private TextField distance;
    @FXML
    private TextField velocity;
    @FXML
    private Label output;
    @FXML
    private ListView<String> view;
    @FXML
    private Button calcButton;

    private Console console;

    @FXML
    private void initialize() {
        console = new Console(view);
    }

    @FXML
    private void handleCalc() {
        final double startHeightVal = Double.parseDouble(startHeight.getText().trim());
        final double endHeightVal = Double.parseDouble(endHeight.getText().trim());
        final double distanceVal = Double.parseDouble(distance.getText().trim());
        final double velocityVal = Double.parseDouble(velocity.getText().trim());

        console.print("Start Height: " + startHeightVal);
        console.print("End Height: " + endHeightVal);
        console.print("Distance: " + distanceVal);
        console.print("Velocity: " + velocityVal);

        console.print("");

        final Task<Void> task = new Task<Void>() {
            @Nullable
            @Override
            protected Void call() {
                calcButton.setDisable(true);
                try {
                    final double angle = Algorithms.calcAngleFrictionAlgorithm(distanceVal, endHeightVal, startHeightVal, velocityVal, 0.0575, 0.0035, 0.45, 1.2041, 8, console);
                    Platform.runLater(() -> output.setText(Double.toString(angle)));
                } catch (PointNotReachableException e) {
                    Platform.runLater(() -> output.setText(e.getMessage()));
                } finally {
                    Platform.runLater(() -> calcButton.setDisable(false));
                }
                return null;
            }
        };
        final Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

}
