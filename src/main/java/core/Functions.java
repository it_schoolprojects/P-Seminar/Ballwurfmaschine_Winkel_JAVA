package core;

import org.jetbrains.annotations.Contract;

public final class Functions {

    //PUBLIC

    @Contract(pure = true)
    public static double calcAngleIdealized(final double x, final double y, final double y_0, final double v_0, boolean alternative) {
        final double g = Constants.g.value;
        final double A = g * Math.pow(x / v_0, 2);
        final double B = Math.pow(y, 2) + Math.pow(y_0, 2) - 2 * y * y_0 + Math.pow(x, 2);
        final double C = Math.pow(g, 2) * Math.pow((x / v_0), 4);

        final double var1 = Math.toDegrees(
                Math.acos(
                        Math.sqrt((
                                -A * y + A * y_0 + Math.pow(x, 2) - Math.sqrt(Math.pow((A * y - A * y_0 - Math.pow(x, 2)), 2) - B * C)
                        ) / (
                                2 * B
                        ))
                ));

        final double var2 = Math.toDegrees(
                Math.acos(
                        Math.sqrt((
                                -A * y + A * y_0 + Math.pow(x, 2) + Math.sqrt(Math.pow((A * y - A * y_0 - Math.pow(x, 2)), 2) - B * C)
                        ) / (
                                2 * B
                        ))
                ));

        if (alternative) {
            final double var = calcAngleIdealized(x, y, y_0, v_0, false);
            if (var == var1)
                return var2;
            else
                return var1;
        }

        if (Double.isNaN(var1))
            return var2;
        if (Double.isNaN(var2))
            return var1;

        if (Math.abs(45 - var1) < Math.abs(45 - var2))
            return var1;
        else
            return var2;
    }

    @Contract(pure = true)
    public static double calcTrajectoryFriction(final double x, double phi, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density) {
        phi = Math.toRadians(phi);
        final double g = Constants.g.value;
        final double C = 0.5 * profile * c_w * density / (m * g);
        final double A = (Math.exp(C * g * x) - 1) / (Math.cos(phi) * Math.sqrt(C) * v_0);
        final double B = Math.atan(Math.sin(phi) * Math.sqrt(C) * v_0);
        final double D = Math.cos(B);

        final double x_u = x_u(phi, v_0, C);

        if (x <= x_u) {
            return Math.log(Math.cos(B - A) / D) / (C * g) + y_0;
        } else
            return -(Math.log((Math.exp(2 * (A - B)) + 1) * D / 2) - A + B) / (C * g) + y_0;
    }

    @Contract(pure = true)
    public static double calcHeightFriction(final double t, double phi, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density) {
        phi = Math.toRadians(phi);
        final double g = Constants.g.value;
        final double C = 0.5 * profile * c_w * density / (m * g);
        final double A = Math.sqrt(C) * g * t;
        final double B = Math.atan(Math.sin(phi) * Math.sqrt(C) * v_0);
        final double D = Math.cos(B);

        final double t_u = t_u(phi, v_0, C);

        if (t <= t_u) {
            return Math.log(Math.cos(B - A) / D) / (C * g) + y_0;
        } else
            return -(Math.log((Math.exp(2 * (A - B)) + 1) * D / 2) - A + B) / (C * g) + y_0;
    }

    //PACKAGE-PRIVATE

    @Contract(pure = true)
    static double calcTrajectoryFrictionDerivedPhi(final double x, double phi, final double v_0, final double m, final double profile, final double c_w, final double density) {
        final double g = Constants.g.value;
        phi = Math.toRadians(phi);
        final double C = 0.5 * profile * c_w * density / (m * g);
        final double x_u = x_u(phi, v_0, C);

        final double S = Math.sin(phi)*Math.sqrt(C)*v_0;
        final double A = Math.atan(S);
        final double A_D = Math.cos(phi) * Math.sqrt(C) * v_0 / (1 + Math.pow(v_0 * Math.sin(phi), 2) * C);
        final double B = (Math.exp(C * g * x) - 1) / (Math.cos(phi) * Math.sqrt(C) * v_0);
        final double B_D = Math.tan(phi) * (Math.exp(C * g * x) - 1) / (Math.cos(phi) * Math.sqrt(C) * v_0);

        if (x <= x_u)
            return (Math.tan(A) * A_D - Math.tan(A - B) * (A_D - B_D)) / (C * g);
        else
            return (A_D * (S - 1) + B_D - 2 * (B_D - A_D) / (1 + 1 / Math.exp(2 * (B - A)))) / (C * g);
    }

    @Contract(pure = true)
    static double t_u(final double phi_rad, final double v_0, final double C) {
        final double g = Constants.g.value;
        return Math.atan(Math.sin(phi_rad) * Math.sqrt(C) * v_0) / (Math.sqrt(C) * g);
    }

    @Contract(pure = true)
    static double calcC(final double profile, final double c_w, final double density, final double m) {
        final double g = Constants.g.value;
        return 0.5 * profile * c_w * density / (m * g);
    }

    //PRIVATE

    private Functions() {
    }

    @Contract(pure = true)
    private static double x_u(final double phi_rad, final double v_0, final double C) {
        final double g = Constants.g.value;
        return Math.log(Math.cos(phi_rad) * Math.sqrt(C) * v_0 * Math.atan(Math.sin(phi_rad) * Math.sqrt(C) * v_0) + 1) / (C * g);
    }

}
