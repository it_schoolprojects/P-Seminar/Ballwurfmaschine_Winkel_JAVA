package core;

enum Constants {
    g(9.81);

    final double value;

    Constants(double x) {
        value = x;
    }
}
