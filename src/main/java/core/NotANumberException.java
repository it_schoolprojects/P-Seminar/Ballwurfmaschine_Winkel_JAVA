package core;

import org.jetbrains.annotations.NotNull;

final class NotANumberException extends Exception {
    final NumberState state;

    NotANumberException(@NotNull final NumberState state) {
        super();
        this.state = state;
    }
}
