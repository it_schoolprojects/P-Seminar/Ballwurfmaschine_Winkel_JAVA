package core;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gui.Console;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;

public final class Algorithms {

    //PUBLIC calcAngleFrictionNewton(x, y, y_0, v_0, m, profile, c_w, density, precision, false, console);

    @Contract(pure = true)
    public static double calcAngleFrictionAlgorithm(final double x, final double y, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density, final int precision, @Nullable final Console console) throws PointNotReachableException {
        try {
            return calcAngleFrictionNewton(x, y, y_0, v_0, m, profile, c_w, density, precision, false, console);
        } catch (LoopException | AngleNotUsableException e) {
            try {
                return calcAngleFrictionNewton(x, y, y_0, v_0, m, profile, c_w, density, precision, true, console);
            } catch (LoopException | AngleNotUsableException a) {
                final String msg = "Calculation failed. Point cannot be aimed.";
                print(msg, console);
                throw new PointNotReachableException(msg);
            } catch (NotANumberException a) {
                if (isInfiniteException(a) && heightReachable(y, y_0, v_0, m, profile, c_w, density))
                    return 90;
                final String msg = "Calculation failed. Point cannot be aimed.";
                print(msg, console);
                throw new PointNotReachableException(msg);
            }
        } catch (NotANumberException e) {
            if (isInfiniteException(e) && heightReachable(y, y_0, v_0, m, profile, c_w, density))
                return 90;
            final String msg = "Calculation failed. Point cannot be aimed.";
            print(msg, console);
            throw new PointNotReachableException(msg);
        }
    }

    //PRIVATE

    private Algorithms() {
    }

    @SuppressFBWarnings("FE_FLOATING_POINT_EQUALITY")
    @Contract(pure = true)
    private static double calcAngleFrictionNewton(final double x, final double y, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density, final int precision, final boolean alternative, @Nullable final Console console) throws LoopException, NotANumberException, AngleNotUsableException {
        double phi_approx = Functions.calcAngleIdealized(x, y, y_0, v_0, alternative); //Startwert ist der Winkel unter idealisierten Bedingungen
        checkNotANumber(phi_approx);
        int precisionLoopCounter = 0;
        int current_precision;

        {
            final double y_approx = Functions.calcTrajectoryFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density);
            current_precision = (int) Math.log10(1 / Math.abs(y_approx - y));
        }

        print("Start: " + phi_approx, console);

        while (current_precision < precision) { //Solange die gewünschte Präzision nicht erreicht ist, mache weiter
            final int prev_precision = current_precision;
            final double phi_approx_prev = phi_approx;
            //Newton-Algorithm
            final double y_approx = Functions.calcTrajectoryFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density);
            phi_approx -= (y_approx - y) / (Functions.calcTrajectoryFrictionDerivedPhi(x, phi_approx, v_0, m, profile, c_w, density)); // phi_n+1 = phi_n - y(phi_n)/y'(phi_n)
            //Checks + logging
            print(phi_approx + " > " + Functions.calcTrajectoryFriction(x, phi_approx, y_0, v_0, m, profile, c_w, density), console);
            current_precision = (int) Math.log10(1 / Math.abs(y_approx - y));
            if (current_precision <= prev_precision)
                precisionLoopCounter++;
            else
                precisionLoopCounter = 0;
            checkNotANumber(phi_approx);
            checkLoop(phi_approx, phi_approx_prev, precisionLoopCounter);
        }

        if (phi_approx > 90 || phi_approx < 0)
            throw new AngleNotUsableException();
        return phi_approx;
    }

    private static void print(@Nullable final String s, @Nullable final Console console) {
        if (console == null)
            System.out.println(s);
        else {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            console.print(s);
        }
    }

    private static void checkNotANumber(final double phi_approx) throws NotANumberException {
        if (Double.isInfinite(phi_approx))
            throw new NotANumberException(NumberState.INFINITE);
        else if (Double.isNaN(phi_approx))
            throw new NotANumberException(NumberState.NAN);
    }

    private static void checkLoop(final double phi_approx, final double phi_approx_prev, final double precisionLoopCounter) throws LoopException {
        if (phi_approx_prev == phi_approx || phi_approx > 500 || phi_approx < -500 || precisionLoopCounter > 500)
            throw new LoopException();
    }

    @Contract(pure = true)
    private static boolean heightReachable(final double y, final double y_0, final double v_0, final double m, final double profile, final double c_w, final double density) {
        final double phi_max = 90;
        final double phi_max_rad = Math.PI / 2; // PI / 2 = 90°
        final double C = Functions.calcC(profile, c_w, density, m);
        final double t_u = Functions.t_u(phi_max_rad, v_0, C);
        final double y_max = Functions.calcHeightFriction(t_u, phi_max, y_0, v_0, m, profile, c_w, density);
        return y <= y_max;
    }

    @Contract(pure = true, value = "null -> false")
    private static boolean isInfiniteException(@Nullable final NotANumberException exception) {
        return exception != null && exception.state == NumberState.INFINITE;
    }

}
