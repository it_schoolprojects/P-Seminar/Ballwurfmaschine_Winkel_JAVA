import core.Functions;
import org.jetbrains.annotations.Contract;
import org.junit.Test;

public final class FunctionsTest {

    //PUBLIC

    @Test
    public void testAngleCalculationVertical() {
        double calculated = Functions.calcAngleIdealized(0, 5, 0, 10, false);
        assert(calculated == 90) : calculated;
        calculated = Functions.calcAngleIdealized(0, -5, 0, 5, false);
        assert(calculated == 90) : calculated;
    }

    @Test
    public void testAngleCalculationNonVertical() {
        double calculated = Functions.calcAngleIdealized(13, 5, 1.5, 13.5, false);
        if (!doubleEquals(calculated, 62.87)) {
            calculated = Functions.calcAngleIdealized(13, 5, 1.5, 13.5, true);
            assert(doubleEquals(calculated, 62.87)) : calculated;
        }
    }

    @Test
    public void testTrajectoryCalculationCase2() {
        final double calculated = Functions.calcTrajectoryFriction(14, 60, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value);
        assert(doubleEquals(calculated, -7.83)) : calculated;
    }

    @Test
    public void testTrajectoryCalculationCase1() {
        final double calculated = Functions.calcTrajectoryFriction(4, 39, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value);
        assert(doubleEquals(calculated, 4.14)) : calculated;
    }

    @Test
    public void testTrajectoryCalculationCaseReversal() {
        final double calculated = Functions.calcTrajectoryFriction(7.4, 39, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value);
        assert(doubleEquals(calculated, 5.06)) : calculated;
    }

    @Test
    public void testHeightCalculationCase1() {
        final double calculated = Functions.calcHeightFriction(0.43, 39, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value);
        assert(doubleEquals(calculated, 4.3)) : calculated;
    }

    @Test
    public void testHeightCalculationCase2() {
        final double calculated = Functions.calcHeightFriction(1.76, 39, 1.5, 15, TestValues.m.value, TestValues.profile.value, TestValues.c_w.value, TestValues.density.value);
        assert(doubleEquals(calculated, 1.11)) : calculated;
    }

    //PRIVATE

    @Contract(pure = true)
    private static boolean doubleEquals(final double d1, final double d2) {
        return Math.abs(d1-d2) < 0.1;
    }

}
