enum TestValues {
    m(0.0575),
    profile(0.014),
    c_w(0.45),
    density(1.2041);

    final double value;

    TestValues(final double val) {
        value = val;
    }
}
